import React, { useReducer } from "react";
import Layout from "./components/layout/layout";
import { text } from "./constants";
import CreditSimulator from "./containers/creditSimulator";
import { TotalAmount } from "./reducers/amount";
import { Term } from "./reducers/term";
import Result from "./containers/result";

export const Context = React.createContext();

export default function App() {
  
  const [AmountState, AmountDispatch] = useReducer(
    TotalAmount.reducer,
    TotalAmount.initialState
  );
  const [TermState, TermDispatch] = useReducer(
    Term.reducer,
    Term.initialState
  );

  // Reducers
  const reducers = {
    AmountState,
    AmountDispatch,
    TermState,
    TermDispatch,
  };


  return (
    <Context.Provider value={reducers}>
      <Layout title={text.title}>
        <CreditSimulator />
        <Result />
      </Layout>
    </Context.Provider>
  );
};