export const tool = (value) => {
     if (value.includes("$")) return value.slice(2).replace(".", "");
     return value.replace(".", "");
};