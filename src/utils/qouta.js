export const FeeByMonth = (amount, term) => {
    if (Number.isNaN(amount) || Number.isNaN(term)) return 0;
    const num = parseInt(amount / term, 10);
    return num.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}