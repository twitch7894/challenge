export function formatNumber(num) {
    const n = num.toString();
    return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".");
};