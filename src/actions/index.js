import { SET_AMOUNT, SET_TERM } from "../constants";

export const setAmount = (value) => ({ type: SET_AMOUNT, payload: value });
export const setTerm = (value) => ({ type: SET_TERM, payload: value });