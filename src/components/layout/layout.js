export default function Layout({ title, children }) {
    return (
      <div className="layout-container">
        <div className="layout-box">
          <div className="layout-resume">
            <h1 className="layout-title">{title}</h1>
            <div className="layout-content">{children}</div>
          </div>
        </div>
      </div>
    );
};