export default function LayoutSlider({ min, max, children, type }) {
    return (
      <div className="layoutslider-container">
        <div className="layoutslider-range">{children}</div>
        <div
          className="layoutslider-range-value"
          style={{ width: `${type !== "term" ? "100%" : "85%"}` }}
        >
          <p className="layoutslider-range-value-p">{min}</p>
          <p className="layoutslider-range-value-p">{max}</p>
        </div>
      </div>
    );
};
  