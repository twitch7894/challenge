export default function Item({ title, children }) {
  return (
    <div className="item-container">
      <div className="item-title">
        <p className="item-title-p">{title}</p>
      </div>
      {children}
    </div>
  );
};