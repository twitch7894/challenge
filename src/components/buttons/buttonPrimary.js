export default function ButtonPrimary({ title }) {
    return (
      <div className="button-primary-container">
        <button className="button-primary">{title}</button>
      </div>
    )
}