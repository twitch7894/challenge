import { SET_AMOUNT, SET_TERM } from "../../constants";
import { tool } from "../../utils/tool";

export default function Value({ value, dispatch, type }) {
  const handleChange = (e) => {
    const value = tool(e.target.value);
    const key = type === "term" ? SET_TERM : SET_AMOUNT;
    if (e.target.value.length <= 11) {
      dispatch({ type: key, payload: value });
    }
  };

  return (
    <div className="value-container">
      <input
        className="input-value"
        value={value}
        onChange={(e) => handleChange(e)}
      />
    </div>
  );
};
