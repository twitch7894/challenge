export default function Qouta({ title, price }) {
    return (
      <div className="qouta-container">
        <div className="qouta-content">
          <div className="qouta-title">
            <p className="qouta-title-p">{title}</p>
          </div>
          <div className="qouta-price">
            <h1 className="qouta-price-h1">$ {price}</h1>
          </div>
        </div>
      </div>
    );
};
  