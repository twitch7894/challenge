/**
 * AMOUNT
 */

 export const MAX_AMOUNT = 50000;
 export const MIN_AMOUNT = 5000;
 export const SET_AMOUNT = "SET_AMOUNT";
 export const INITIAL_AMOUNT = "INITIAL_AMOUNT";

 /**
  * TERM
  */

  export const MAX_TERM = 24;
  export const MIN_TERM = 3;
  export const SET_TERM = "GET_TERM";
  export const INITIAL_TERM = "INITIAL_TERM";

/**
 * TEXT
 */

export const text = {
    title: "Simulá tu crédito",
    amount: "MONTO TOTAL",
    term: "PLAZO",
    quota: "COUTA FIJA POR MES",
    button_primary: "OBTENÉR CRÉDITO",
    button_secondary: "VER DETALLE DE COUTAS",
};