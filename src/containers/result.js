import { useContext } from "react";
import Qouta from "../components/cards/qouta";
import { text } from "../constants";
import { Context } from "../App";
import { FeeByMonth } from "../utils/qouta";
import ButtonPrimary from "../components/buttons/buttonPrimary";
import ButtonSecondary from "../components/buttons/buttonSecondary";

export default function Result() {
  const { AmountState, TermState } = useContext(Context);
  return (
    <div className="result-container">
      <Qouta
        title={text.quota}
        price={FeeByMonth(AmountState.data, TermState.data)}
      />
      <div className="result-button">
        <ButtonPrimary title={text.button_primary} />
        <ButtonSecondary title={text.button_secondary} />
      </div>
    </div>
  );
};
