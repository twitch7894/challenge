import { useContext } from "react";
import { Context } from "../App";
import Item from "../components/items/item";
import Value from "../components/cards/value";
import Slider from "rc-slider";
import { formatNumber } from "../utils/formatNumber";
import { text, MAX_TERM, MIN_TERM,MAX_AMOUNT, MIN_AMOUNT } from "../constants";
import LayoutSlider from "../components/layout/layoutSlider";
import Content from "../components/layout/content";
import { setAmount, setTerm } from "../actions";

export default function CreditSimulator() {
  const {
    AmountState,
    AmountDispatch,
    TermState,
    TermDispatch,
  } = useContext(Context);

  return (
    <div className="CreditSimulator-container">
      <Content>
        <Item title={text.amount}>
          <Value
            value={`$ ${formatNumber(AmountState.data)}`}
            dispatch={AmountDispatch}
            type={"amout"}
          />
        </Item>
        <LayoutSlider
          min={`$ ${formatNumber(MIN_AMOUNT)}`}
          max={`$ ${formatNumber(MAX_AMOUNT)}`}
          type={"amount"}
        >
          <Slider
            className="slider"
            min={MIN_AMOUNT}
            max={MAX_AMOUNT}
            step={1}
            onChange={(value) =>
              AmountDispatch(setAmount(value))
            }
          />
        </LayoutSlider>
      </Content>
      <Content>
        <Item title={text.term}>
          <Value
            value={`${formatNumber(TermState.data)}`}
            dispatch={TermDispatch}
            type={"term"}
          />
        </Item>
        <LayoutSlider
          min={`${formatNumber(MIN_TERM)}`}
          max={`${formatNumber(MAX_TERM)}`}
          type={"term"}
        >
          <Slider
            className="slider"
            min={MIN_TERM}
            max={MAX_TERM}
            step={1}
            onChange={(value) =>
              TermDispatch(setTerm(value))
            }
          />
        </LayoutSlider>
      </Content>
    </div>
  );
}
