import { SET_AMOUNT, INITIAL_AMOUNT, MIN_AMOUNT } from "../constants";

const initialState = {
  data: MIN_AMOUNT
};

const reducer = (state, action) => {
  switch (action.type) {
    case SET_AMOUNT:
      return {
        data: action.payload,
      };
    case INITIAL_AMOUNT:
      return initialState;
    default:
      return state;
  }
};

export const TotalAmount = { initialState, reducer };
