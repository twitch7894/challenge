import { SET_TERM, INITIAL_TERM, MIN_TERM } from "../constants";

const initialState = {
  data: MIN_TERM
};

const reducer = (state, action) => {
  switch (action.type) {
    case SET_TERM:
      return {
        data: action.payload,
      };
    case INITIAL_TERM:
      return initialState;
    default:
      return state;
  }
};

export const Term = { initialState, reducer };